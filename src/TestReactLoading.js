import React from 'react';
import ReactLoading from 'react-loading';

class TestReactLoading extends React.Component {
    render() {
        return <ReactLoading type={"spin"} color={"grey"} height={'20%'} width={'20%'}/>
    }
}

export default TestReactLoading;