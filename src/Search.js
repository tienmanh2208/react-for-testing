import React from "react";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            submit: ''
        }
    }

    handleInputChange(e) {
        this.setState({
            input: e.target.value,
        })
    }

    fetchData() {
        this.props.fetchData(this.state.input, 'repositories');
    }

    render() {
        let searchStyle = {
            width: "70%",
            margin: "auto",
            marginTop: 50,
        };

        return (
            <div style={searchStyle}>
                <div style={{fontSize: 30, marginBottom: 10}}>
                    <i className="fas fa-search" style={{marginRight: 10}}/>Search more than <strong>50M</strong> users
                </div>
                <div>
                    <input value={this.state.input} onChange={this.handleInputChange.bind(this)}
                           style={{width: "88%", height: "25px", paddingLeft: "5px"}} placeholder={"Search GitHub"}/>
                    <button onClick={this.fetchData.bind(this)} style={{width: "10%", float: "right", height: "31px"}}>
                        <strong>Search</strong>
                    </button>
                </div>
                <div style={{marginTop: "5px", fontSize: "13px"}}>This project belongs to TM</div>
            </div>
        );
    }
}

export default Search;