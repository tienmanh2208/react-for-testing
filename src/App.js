import React from 'react';
import Search from "./Search";
import Content from "./Content";
import Axios from "axios";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyWords: null,
            type: null,
            data: null,
        }
    }

    fetchData(keyWords, type) {
        Axios.get('https://api.github.com/search/' + type + '?q=' + keyWords)
            .then(res => {
                this.setState({
                    keyWords: keyWords,
                    type: type,
                    data: res.data
                });
            })
    }

    render() {
        return (
            <div>
                <Search fetchData={this.fetchData.bind(this)}/>
                {this.state.data ? <Content currentCatalogue={this.state.type} data={this.state.data}/> : <div/>}
            </div>
        );
    }
}

export default App;