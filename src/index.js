import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import AppWrapper from "./TestRedux";
import TestRoute from "./TestRoute";
import Example from "./TestReactLoading";
import TestBackground from "./TestBackground";
import * as serviceWorker from './serviceWorker';
import TestNestingRoute from './TestNestingRoute';

ReactDOM.render(<TestNestingRoute />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
