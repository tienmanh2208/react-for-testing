import React from "react";
import ParticlesBg from "particles-bg";

class TestBackground extends React.Component {
    render() {
        return (
            <div>
                <ParticlesBg type="cobweb" bg={true} color="#2f3492"/>
            </div>
        );
    }
}

export default TestBackground;