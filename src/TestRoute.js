import React from "react";
import {BrowserRouter as Router, Link, Route, Switch, Redirect, useHistory, useLocation} from "react-router-dom";

class TestRoute extends React.Component {
    render() {
        return (
            <Router>
                <AuthButton />
                <div>
                    <ul>
                        <li>
                            <Link to={"/public"}>Public page</Link>
                        </li>
                        <li>
                            <Link to={"/protected"}>Protected page</Link>
                        </li>
                    </ul>
                </div>

                <Switch>
                    <Route path={"/public"}>
                        <Public/>
                    </Route>
                    <Route path={"/login"}>
                        <Login/>
                    </Route>
                    <PrivateRoute path={"/protected"}>
                        <Protected/>
                    </PrivateRoute>
                </Switch>
            </Router>
        );
    }
}

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
        fakeAuth.isAuthenticated = true;
        setTimeout(cb, 100); // fake async
    },
    signout(cb) {
        fakeAuth.isAuthenticated = false;
        setTimeout(cb, 100);
    }
};

function AuthButton() {
    let history = useHistory();

    return fakeAuth.isAuthenticated ? (
        <p>
            Welcome!{" "}
            <button onClick={() => {fakeAuth.signout(() => history.push("/"));}}>
                Sign out
            </button>
        </p>
    ) : (
        <p>You are not logged in.</p>
    );
}

const PrivateRoute = ({children, ...rest}) => {
    return (
        <Route
            {...rest}
            render={({location}) =>
                fakeAuth.isAuthenticated ? (
                    console.log(location),
                    children
                ) : (
                    console.log(location),
                    <Redirect to={{pathname: "/login", state: {from: location}}}/>
                )
            }
        />
    )
};

const Public = () => {
    return (
        <h1>Public page</h1>
    );
};
const Protected = () => {
    return (
        <h1>Protected page</h1>
    );
};

const Login = () => {
    let history = useHistory();
    let location = useLocation();

    let { from } = location.state || { from: { pathname: "/" } };
    let login = () => {
        fakeAuth.authenticate(() => {
            history.replace(from);
        });
    };

    return (
        <div>
            <p>You must log in to view the page at {from.pathname}</p>
            <button onClick={login}>Log in</button>
        </div>
    );
};

export default TestRoute;