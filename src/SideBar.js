import React from "react";
import Content from "./Content";

class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTypes: [
                {
                    name: "Repositories",
                    quantity: 1000
                },
                {
                    name: "Code",
                    quantity: 0
                },
                {
                    name: "Commits",
                    quantity: 0
                },
                {
                    name: "Issues",
                    quantity: 0
                },
                {
                    name: "Packages",
                    quantity: 0
                },
                {
                    name: "MarketPlace",
                    quantity: 0
                },
                {
                    name: "Topics",
                    quantity: 0
                },
                {
                    name: "Wikis",
                    quantity: 0
                },
                {
                    name: "Users",
                    quantity: 0
                }
            ],
            languages: [
                {
                    name: "Java",
                    quantity: 1000
                },
                {
                    name: "HTML",
                    quantity: 0
                },
                {
                    name: "Javascript",
                    quantity: 0
                },
                {
                    name: "Python",
                    quantity: 0
                },
                {
                    name: "Makefile",
                    quantity: 0
                },
                {
                    name: "PHP",
                    quantity: 0
                },
                {
                    name: "CSS",
                    quantity: 0
                },
                {
                    name: "C++",
                    quantity: 0
                },
                {
                    name: "C#",
                    quantity: 0
                },
                {
                    name: "Shell",
                    quantity: 0
                }
            ],
        };
        this.updateStateSearchTypes.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextState.currentSearchType !== this.state.currentSearchType;
    }

    handleTypeClick(name) {
        console.log('Sidebar.js 97 handle type click');
        this.props.changeCatalogue(name);
    }

    render() {
        this.updateStateSearchTypes(this.props.pageName, this.props.data ? this.props.data.total_count : 0);
        let catalogueSearch = this.state.searchTypes.map((info, index) => {
            return <SubMenu index={index} name={info.name} quantity={info.quantity}
                            handleClick={this.handleTypeClick.bind(this)}/>
        });

        let catalogueLanguages = this.state.languages.map((info) => {
            return <div style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                flexWrap: "nowrap",
                height: "20px",
                padding: "0px 25px",
                fontSize: "13px",
            }}>
                <div style={{color: "#536B8F"}}>
                    {info.name}
                </div>
                <div style={{color: "#586069"}}>
                    {info.quantity}
                </div>
            </div>
        });

        return (
            <div style={{width: "25%", float: "left"}}>
                {catalogueSearch}
                <div style={{
                    borderWidth: "1px",
                    borderStyle: "solid",
                    borderColor: "#E1E4E8",
                    marginTop: "15px",
                    paddingBottom: "20px"
                }}>
                    <div style={{
                        margin: "20px 15px"
                    }}><strong>Languages</strong></div>
                    {catalogueLanguages}
                </div>
            </div>
        );
    }

    updateStateSearchTypes(name, quantity) {
        let tmpState = this.state.searchTypes;

        for(let i = 0; i < tmpState.length; ++i) {
            if(name === 'repositories') {
                tmpState[i].quantity = quantity;
                break;
            }
        }

        this.setState({
            searchTypes: tmpState
        })
    }
}

class SubMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false,
            name: props.name,
            quantity: props.quantity,
            index: props.index
        };
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextState.hover !== this.state.hover;
    }

    handleMouseOver() {
        this.setState({
            hover: true,
            name: this.state.name,
            quantity: this.state.quantity,
        })
    }

    handleMouseOut() {
        this.setState({
            hover: false,
            name: this.state.name,
            quantity: this.state.quantity,
        })
    }

    handleClick() {
        console.log('Sidebar.js 193 handle click');
        this.props.handleClick(this.state.name.toLowerCase())
    }

    render() {
        let catalogueStyle = {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            flexWrap: "nowrap",
            height: "35px",
            borderWidth: "1px 1px 0.5px 1px",
            borderColor: "#E1E4E8",
            borderStyle: "solid",
            cursor: "pointer"
        };

        if (this.state.hover) {
            catalogueStyle.backgroundColor = "#F6F8FA";
        }

        return (
            <div style={catalogueStyle}
                 key={this.state.key}
                 onMouseOver={this.handleMouseOver.bind(this)}
                 onMouseOut={this.handleMouseOut.bind(this)}
                 onClick={this.handleClick.bind(this)}>
                <div style={{
                    paddingLeft: "10px",
                    lineHeight: "35px",
                    fontSize: "15px",
                    color: "#5583E5"
                }}>{this.state.name}</div>
                <div style={{
                    marginRight: "10px",
                    lineHeight: "35px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                }}>
                    <div style={{
                        lineHeight: "20px",
                        fontSize: "13px",
                        backgroundColor: "#6A737D",
                        color: "white",
                        padding: "0px 5px",
                        borderRadius: "10px"
                    }}>
                        {this.state.quantity}
                    </div>
                </div>
            </div>
        );
    }
}

export default SideBar;