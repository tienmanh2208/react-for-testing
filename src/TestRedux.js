import React from "react";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {connect} from "react-redux";

const SEARCH = "search";

const searchText = (text) => {
    return {
        type: SEARCH,
        content: text,
    }
};

const searchReducer = (state = {list_result: {}}, action) => {
    switch (action.type) {
        case SEARCH:
            let tmp = state.list_result;
            tmp[action.content] = Math.random();
            return {
                list_result: tmp
            };
        default:
            return state;
    }
};

const store = createStore(searchReducer);

class TestRedux extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            input: e.target.value,
        })
    }

    handleSubmit() {
        console.log('submit');
        this.props.searchFromText(this.state.input);
        this.setState({
            input: ''
        })
    }

    render() {
        console.log(this.props.list_result);
        return <div>
            <h1>Test React Redux</h1>
            <input value={this.state.input} onChange={this.handleChange}/>
            <button onClick={this.handleSubmit}>Submit</button>
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        list_result: state.list_result
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        searchFromText: (message) => {
            dispatch(searchText(message));
        }
    }
};

const Container = connect(mapStateToProps, mapDispatchToProps)(TestRedux);

class AppWrapper extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Container/>
            </Provider>
        );
    }
}

export default AppWrapper;