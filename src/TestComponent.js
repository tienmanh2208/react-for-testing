import React from 'react';

class TestComponent extends React.Component {
    render() {
        return (
            <h3>This is testing component</h3>
        );
    }
}

export default TestComponent;