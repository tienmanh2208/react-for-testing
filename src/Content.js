import React from "react";
import SideBar from "./SideBar";
import MainContent from "./MainContent";

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCatalogue: 'repositories',
        };
        this.changeCurrentCatalogue.bind(this);
    }

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     console.log('Content.js 16 check should update or not');
    //     console.log(this.state);
    //     console.log(nextState);
    //     console.log(nextProps);
    //     console.log(nextState.currentCatalogue !== this.state.currentCatalogue
    //         || nextProps.currentCatalogue !== this.state.currentCatalogue);
    //     return nextState.currentCatalogue !== this.state.currentCatalogue
    //         || nextProps.currentCatalogue !== this.state.currentCatalogue;
    // }

    changeCurrentCatalogue(name) {
        console.log('Content.js 27 change state');
        console.log(this.state);
        console.log(name);
        console.log('=========');
        this.setState({
            currentCatalogue: name,
        });
    }

    render() {
        console.log("Content.js 36 Render");
        return (
            <div style={{width: "70%", margin: "auto", marginTop: 20}}>
                <SideBar pageName={this.state.currentCatalogue} data={this.props.data} changeCatalogue={this.changeCurrentCatalogue.bind(this)}/>
                <MainContent pageName={this.state.currentCatalogue} data={this.props.data}/>
            </div>
        );
    }
}

export default Content;