import React from "react";

class MainContent extends React.Component {
    pageTypes = Object.freeze({
        repositories: "repositories",
        code: "code",
        commits: "commits",
        issues: "issues",
        packages: "packages",
        marketPlace: "marketplace",
        topics: "topics",
        wikis: "wikis",
        users: "users"
    });

    render() {
        console.log('MainContent.js 28 Render main content');
        let content = null;
        console.log(this.props.pageName);

        switch (this.props.pageName) {
            case this.pageTypes.repositories:
                content = <h2>This is repositories</h2>;
                break;
            case this.pageTypes.code:
                content = <h2>This is code</h2>;
                break;
            case this.pageTypes.commits:
                content = <h2>This is commits</h2>;
                break;
            case this.pageTypes.issues:
                content = <h2>This is issues</h2>;
                break;
            case this.pageTypes.packages:
                content = <h2>This is packages</h2>;
                break;
            case this.pageTypes.marketPlace:
                content = <h2>This is marketPlace</h2>;
                break;
            case this.pageTypes.wikis:
                content = <h2>This is wikis</h2>;
                break;
            case this.pageTypes.users:
                content = <h2>This is users</h2>;
                break;
            case this.pageTypes.topics:
                content = <h2>This is topics</h2>;
                break;
                defalt:
                    content = <h2>This is Default</h2>;
                break;
        }

        return (
            <div style={{width: "75%", float: "right"}}>
                <h2>{content}</h2>
            </div>
        );
    }
}

export default MainContent;